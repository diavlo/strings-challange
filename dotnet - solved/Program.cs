﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dotnet
{
    class Program
    {
        static void Main(string[] args)
        {
            // The countdown has started! You have 5 minutes to find the abort code using .net
            // Make this program return the correct abort code and save yourself from getting shot
            //
            // Create an algorithm to ROT-2 transform this quote.
            // ROT-2 is transforming the characters [a-z] 2 places in ASCII.
            // After character z characters rotates from beginning (char a).
            // Character ' ' doesnt change. Example:
            // - 'a' becomes 'c'
            // - 'z' becomes 'b'
            // - ' ' becomes ' '

            var quote = "always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you livez";
            var rot_quote = string.Empty;


            // /----------------------/
            // / Your code goes here! /
            // /----------------------/
            foreach (var charr in quote)
            {
                var i = (int) charr;
                i = i - 97;
                if (i >= 0 && i <= 26)
                {
                    i += 2;
                    i = i % 26;
                }

                i += 97;

                rot_quote += (char) i;
            }


            // The final abortcode
            var abortCode = CreateMD5(rot_quote);
			System.IO.File.WriteAllText("c:\\temp\\abortcode-"+abortCode+".txt", "ABORT LAUNCH");
			

            Console.WriteLine(rot_quote);
            Console.ReadLine();
        }
		
		public static string CreateMD5(string input)
		{
			// Use input string to calculate MD5 hash
			using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
			{
				byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
				byte[] hashBytes = md5.ComputeHash(inputBytes);

				// Convert the byte array to hexadecimal string
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < hashBytes.Length; i++)
				{
					sb.Append(hashBytes[i].ToString("X2"));
				}
				return sb.ToString();
			}
		}
    }
}
