var crypto = require('crypto');
var fs = require('fs');

//  The countdown has started! You have 5 minutes to find the abort code using nodejs
//  Make this program return the correct abort code and save yourself from getting shot
  
//   Create an algorithm to ROT-2 transform this quote.
//   ROT-2 is transforming the characters [a-z] 2 places in ASCII.
//   After character z characters rotates from beginning (char a).
//   Character ' ' doesnt change. Example:
//   - 'a' becomes 'c'
//   - 'z' becomes 'b'
//   - ' ' becomes ' '

var quote = "always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you livez"
var rot_quote = ""
var arr = quote.split('');
arr.forEach(c => {
    if (c===' ') {
        rot_quote += ' '
    } else if (c==='z') {
        rot_quote += 'b'
    } else if (c==='y') {
        rot_quote += 'a'
    } else {
        rot_quote += String.fromCharCode(c.charCodeAt(0)+2);
    }
});

// The final abortcode
// Output file, do not edit this
var abortCode = crypto.createHash('md5').update(rot_quote).digest("hex");

fs.writeFile('c://temp/abortcode-' + abortCode + '.txt', 'ABORT LAUNCH', function (err) {
    if (err) throw err;
    console.log(rot_quote);
});
