package main

import (
	"fmt"
	"io/ioutil"
	"crypto/md5"
)

func main() {
	// The countdown has started! You have 5 minutes to find the abort code using golang
	// Make this program return the correct abort code and save yourself from getting shot
	//
	// Create an algorithm to ROT-2 transform this quote.
	// ROT-2 is transforming the characters [a-z] 2 places in ASCII.
	// After character z characters rotates from beginning (char a).
	// Character ' ' doesnt change. Example:
	// - 'a' becomes 'c'
	// - 'z' becomes 'b'
	// - ' ' becomes ' '

	quote := "always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you livez";
	rotQuote := "";

	for _, element := range quote {
		if element == 'z' {
			rotQuote += "b";
		} else if element == 'y' {
			rotQuote += "a";
		} else if element == ' ' {
			rotQuote += " ";
		} else {
			rotQuote += string(element + 2);
		}
	}

	
	//////////////////////////////
	// END                       /
	//////////////////////////////

	// The final abortcode
	abortCode := DigestString(rotQuote);
	fmt.Println(rotQuote);
	fmt.Println(abortCode);

	ioutil.WriteFile("c://temp/abortcode-"+abortCode+".txt", []byte("ABORT LAUNCH"), 0644)
}

func DigestString(s string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(s)))
}